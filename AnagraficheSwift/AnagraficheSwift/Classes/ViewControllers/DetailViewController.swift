//
//  DetailViewController.swift
//  AnagraficheSwift
//
//  Created by Damiano Giusti on 20/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

import UIKit

import Former
import RealmSwift

protocol DetailViewControllerDelegate : class {
    func detailViewControllerDidSaveContact()
}

class DetailViewController: FormViewController {
    
    var contact : Contact?
    weak var delegate : DetailViewControllerDelegate?
    
    
    private var contactRepository = ContactRepositoryFactory.defaultRepository()
    private var didSave = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if let contact = self.contact {
            self.title = contact.description
        }
        
        self.prepareForm()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        DispatchQueue.global().async { [weak self] in
            if let strongSelf = self {
                if (strongSelf.didSave) {
                    strongSelf.delegate?.detailViewControllerDidSaveContact()
                }
            }
        }
    }
    
    
    // MARK: - Form
    
    
    private func prepareForm() -> Void {
        guard let contact = self.contact else { return }
        
        let nameRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.titleLabel.text = "Name"
            }
            .configure { (row: TextFieldRowFormer<FormTextFieldCell>) in
                row.placeholder = "Name..."
                row.text = contact.name
            }
            .onTextChanged(self.saveText(property: "name"))
        
        let surnameRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.titleLabel.text = "Surname"
            }
            .configure { (row: TextFieldRowFormer<FormTextFieldCell>) in
                row.placeholder = "Surname..."
                row.text = contact.surname
            }
            .onTextChanged(self.saveText(property: "surname"))
        
        let birthDateRow = InlineDatePickerRowFormer<FormInlineDatePickerCell>(cellSetup: { (cell: FormInlineDatePickerCell) in
            cell.titleLabel.text = "Birth date"
        }).configure { (row: InlineDatePickerRowFormer<FormInlineDatePickerCell>) in
            row.date = contact.birthDate ?? Date()
        }
        
        let emailRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.titleLabel.text = "Email"
            }
            .configure { (row: TextFieldRowFormer<FormTextFieldCell>) in
                row.placeholder = "Email..."
                row.text = contact.email ?? ""
            }
            .onTextChanged(self.saveText(property: "email"))
        
        let phoneNumberRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.titleLabel.text = "Phone number"
            }
            .configure { (row: TextFieldRowFormer<FormTextFieldCell>) in
                row.placeholder = "Phone number..."
                row.text = contact.phoneNumber ?? ""
            }
            .onTextChanged(self.saveText(property: "phoneNumber"))
        
        let addressRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.titleLabel.text = "Address"
            }
            .configure { (row: TextFieldRowFormer<FormTextFieldCell>) in
                row.placeholder = "Address..."
                row.text = contact.address ?? ""
            }
            .onTextChanged(self.saveText(property: "address"))
        
        let addressNumberRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.titleLabel.text = "Address number"
            }
            .configure { (row: TextFieldRowFormer<FormTextFieldCell>) in
                row.placeholder = "Address number..."
                row.text = contact.addressNumber ?? ""
            }
            .onTextChanged(self.saveText(property: "addressNumber"))
        
        let cityRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.titleLabel.text = "City"
            }
            .configure { (row: TextFieldRowFormer<FormTextFieldCell>) in
                row.placeholder = "City..."
                row.text = contact.city ?? ""
            }
            .onTextChanged(self.saveText(property: "city"))
        
        let zipRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.titleLabel.text = "ZIP"
            }
            .configure { (row: TextFieldRowFormer<FormTextFieldCell>) in
                row.placeholder = "Zip code..."
                row.text = contact.zipCode ?? ""
            }
            .onTextChanged(self.saveText(property: "zipCode"))
        
        let provinceRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.titleLabel.text = "Province"
            }
            .configure { (row: TextFieldRowFormer<FormTextFieldCell>) in
                row.placeholder = "Province..."
                row.text = contact.province ?? ""
            }
            .onTextChanged(self.saveText(property: "province"))
        
        let latitudeRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.titleLabel.text = "Latitude"
            }
            .configure { (row: TextFieldRowFormer<FormTextFieldCell>) in
                row.placeholder = "Latitude..."
                row.text = contact.latitude ?? ""
            }
            .onTextChanged(self.saveText(property: "latitude"))
        
        let longitudeRow = TextFieldRowFormer<FormTextFieldCell>() {
            $0.titleLabel.text = "Longitude"
            }
            .configure { (row: TextFieldRowFormer<FormTextFieldCell>) in
                row.placeholder = "Longitude..."
                row.text = contact.longitude ?? ""
            }
            .onTextChanged(self.saveText(property: "longitude"))
        
        let mainSection = SectionFormer(rowFormer: nameRow, surnameRow, birthDateRow, emailRow, phoneNumberRow, addressRow, addressNumberRow, cityRow, zipRow, provinceRow, latitudeRow, longitudeRow)
        self.former.append(sectionFormer: mainSection)
        
    }
    
    
    // MARK: - Persistence
    
    
    private func saveText(property: String) -> ((String) -> Void) {
        
        self.didSave = true
        
        return { (text: String) in
            if let contact = self.contact {
                self.contactRepository.update(contact: contact, actions: { (c) in
                    c.setValue(text, forKeyPath: property)
                    NSLog("%@", text)
                })
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
