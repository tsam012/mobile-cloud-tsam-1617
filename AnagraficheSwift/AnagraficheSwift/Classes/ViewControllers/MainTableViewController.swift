//
//  ViewController.swift
//  AnagraficheSwift
//
//  Created by Damiano Giusti on 20/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

import UIKit

import RealmSwift

class MainTableViewController: UITableViewController, DetailViewControllerDelegate {
    
    private var contacts : Array<Contact> = []
    private var contactsRepository : ContactsRepository = ContactRepositoryFactory.defaultRepository()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.fetchContacts()
        
        self.title = "Anagrafiche"
    }
    
    
    // MARK: - Data

    
    private func fetchContacts() -> Void {
        
        // if no contacts are persisted, generate random contacts
        if self.contactsRepository.count() == 0 {
            self.contactsRepository.saveAll(contacts: DataSourceService.randomContacts(count: 10))
            NSLog("Contacts generated")
        }
        
        self.contacts = self.contactsRepository.findAll()
        self.tableView.reloadData()
    }
    
    
    // MARK: - Table View
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count;
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell", for: indexPath);
        
        let contact = contacts[indexPath.row]
        
        cell.textLabel?.text = contact.description
        
        return cell
    }
    
    
    
    
    
    // MARK: - Delegation
    
    
    func detailViewControllerDidSaveContact() {
        self.tableView.reloadData()
    }
    
    
    // MARK: - Navigation
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: DetailViewController.self) {
            let detailViewController = segue.destination as! DetailViewController
            
            if let indexPath = self.tableView.indexPathForSelectedRow {
                detailViewController.contact = contacts[indexPath.row]
                detailViewController.delegate = self
            } else {
                NSLog("%@", "Error on detail view controller segue")
            }
        }
    }
    
}

