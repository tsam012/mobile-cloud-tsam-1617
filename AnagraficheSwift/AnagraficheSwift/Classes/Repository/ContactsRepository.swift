//
//  ContactsRepository.swift
//  AnagraficheSwift
//
//  Created by Damiano Giusti on 21/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

import UIKit

protocol ContactsRepository {
    
    func count() -> Int
    
    func findAll() -> Array<Contact>
    
    func findBy(primaryKey: String) -> Contact?
    
    func save(contact: Contact) -> Void
    
    func saveAll(contacts: Array<Contact>) -> Void
    
    func update(contact: Contact, actions: ((Contact) -> ())) -> Void
    
    func deleteBy(primaryKey: String) -> Void

}
