//
//  ContactRepositoryFactory.swift
//  AnagraficheSwift
//
//  Created by Damiano Giusti on 21/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

import UIKit

class ContactRepositoryFactory: NSObject {
    
    static func defaultRepository() -> ContactsRepository {
        return RealmContactRepository()
    }

}
