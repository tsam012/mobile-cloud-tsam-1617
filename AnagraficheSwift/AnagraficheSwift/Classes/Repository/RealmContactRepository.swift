//
//  RealmContactRepository.swift
//  AnagraficheSwift
//
//  Created by Damiano Giusti on 21/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

import UIKit

import RealmSwift

class RealmContactRepository : ContactsRepository {
    
    var realm : Realm
    
    init() {
        realm = try! Realm()
    }
    
    
    // MARK: - Delegate
    
    
    func count() -> Int {
        return realm.objects(Contact.self).count
    }
    
    
    func findAll() -> Array<Contact> {
        return Array(realm.objects(Contact.self))
    }
    
    
    func findBy(primaryKey: String) -> Contact? {
        return realm.object(ofType: Contact.self, forPrimaryKey: primaryKey)
    }
    
    
    func save(contact: Contact) -> Void {
        realm.beginWrite()
        realm.add(contact, update: false)
        do {
            try realm.commitWrite()
        } catch {
            //
        }
    }
    
    
    func saveAll(contacts: Array<Contact>) {
        realm.beginWrite()
        realm.add(contacts)
        do {
            try realm.commitWrite()
        } catch {
            //
        }
    }
    
    
    func update(contact: Contact, actions: ((Contact) -> ())) -> Void {
        realm.beginWrite()
        actions(contact)
        do {
            try realm.commitWrite()
        } catch {
            //
        }
    }
    
    
    func deleteBy(primaryKey: String) -> Void {
        realm.beginWrite()
        if let contact = self.findBy(primaryKey: primaryKey) {
            realm.delete(contact)
        }
        do {
            try realm.commitWrite()
        } catch {
            //
        }
    }
    
}
