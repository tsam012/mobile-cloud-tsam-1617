//
//  DataSourceService.swift
//  AnagraficheSwift
//
//  Created by Damiano Giusti on 20/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

import UIKit

class DataSourceService: NSObject {
    
    /*
     + (Contact *)contact {
     return [[Contact alloc] initWithId:RandomUUID];
     }
     
     + (NSMutableArray *)randomContacts:(NSInteger)number {
     NSMutableArray *contacts = [[NSMutableArray alloc] init];
     
     for (int i = 0; i < number; ++i) {
     Contact *contact = [[Contact alloc] initWithId:RandomUUID];
     
     [contact setName:[NSString stringWithFormat:@"Name #%i", (i+1)]];
     [contact setSurname:[NSString stringWithFormat:@"Surname #%i", (i+1)]];
     [contact setBirthDate:[NSDate date]];
     
     [contacts addObject:contact];
     }
     
     return contacts;
     }
 */
    
    static func randomContact() -> Contact {
        return Contact(uuid: NSUUID().uuidString)
    }
    
    
    static func randomContacts(count: Int) -> Array<Contact> {
        var contacts : Array<Contact> = []
        
        for i in 0..<count {
            let contact = Contact(uuid: NSUUID().uuidString)
            
            contact.name = "Name #\(i + 1)"
            contact.surname = "Surname #\(i + 1)"
            contact.birthDate = Date()
            
            contacts.append(contact)
        }
        
        return contacts
    }

}
