//
//  DebounceWriteService.swift
//  AnagraficheSwift
//
//  Created by Damiano Giusti on 20/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

import UIKit

import RealmSwift

class DebounceWriteService: NSObject {
    
    var lastFireTime : DispatchTime
    let dispatchDelay : DispatchTimeInterval
    
    init(delay: Int) {
        self.lastFireTime = DispatchTime.now()
        self.dispatchDelay = DispatchTimeInterval.milliseconds(delay)
    }
    
    func debounce(realm: Realm, queue: DispatchQueue, action: @escaping ((Realm)->())) -> Void {
        let dispatchTime: DispatchTime = lastFireTime + dispatchDelay
        queue.asyncAfter(deadline: dispatchTime, execute: { [weak self] in
            
            guard let strongSelf = self else { return }
            
            let when: DispatchTime = strongSelf.lastFireTime + strongSelf.dispatchDelay
            let now = DispatchTime.now()
            if now.rawValue >= when.rawValue {
                strongSelf.lastFireTime = DispatchTime.now()
                realm.beginWrite()
                action(realm)
                do {
                    try realm.commitWrite()
                } catch {
                    //
                }
            }
        })
    }

}
