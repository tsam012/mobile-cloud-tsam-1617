//
//  Contact.swift
//  AnagraficheSwift
//
//  Created by Damiano Giusti on 20/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

import UIKit
import RealmSwift

class Contact : Object {
    
    dynamic var id : String?
    
    dynamic var name : String?
    
    dynamic var surname : String?
    
    dynamic var birthDate : Date?

    dynamic var email : String?
    
    dynamic var phoneNumber : String?
    
    dynamic var address : String?
    
    dynamic var addressNumber : String?
    
    dynamic var city : String?
    
    dynamic var zipCode : String?

    dynamic var province : String?
    
    dynamic var latitude : String?
    
    dynamic var longitude : String?
    
    convenience init(uuid: String) {
        self.init()
        self.id = uuid
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    override var description: String {
        return (self.name ?? "") + " " + (self.surname ?? "")
    }
}
