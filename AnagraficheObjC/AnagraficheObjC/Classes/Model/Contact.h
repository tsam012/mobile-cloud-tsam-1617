//
//  Contact.h
//  AnagraficheObjC
//
//  Created by Damiano Giusti on 16/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FXForms.h"

@interface Contact : NSObject <FXForm>

@property (strong, nonatomic, readonly) NSString *contactId;

@property (strong, nonatomic) NSString *name;

@property (strong, nonatomic) NSString *surname;

@property (strong, nonatomic) NSDate *birthDate;

@property (strong, nonatomic) NSString *email;

@property (strong, nonatomic) NSString *phoneNumber;

@property (strong, nonatomic) NSString *address;

@property (strong, nonatomic) NSString *addressNumber;

@property (strong, nonatomic) NSString *zipCode;

@property (strong, nonatomic) NSString *province;

@property (nonatomic) double latitude;

@property (nonatomic) double longitude;

- (instancetype)initWithId:(NSString *)contactId;

- (NSString *)displayName;

@end
