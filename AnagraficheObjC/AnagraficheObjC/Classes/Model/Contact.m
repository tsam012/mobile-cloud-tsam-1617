//
//  Contact.m
//  AnagraficheObjC
//
//  Created by Damiano Giusti on 16/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "Contact.h"


@implementation Contact

- (instancetype)initWithId:(NSString *)contactId {
    self = [super init];
    
    if (self) {
        _contactId = contactId;
    }
    
    return self;
}

- (NSString *)displayName {
    
    NSString  *_displayName = @"";
    if (self.name) {
        _displayName = [_displayName stringByAppendingString:self.name];
    }
    if (self.surname) {
        if (![_displayName isEqualToString:@""]) {
            _displayName = [_displayName stringByAppendingString:[NSString stringWithFormat:@" %@", self.surname]];
        } else {
            _displayName = [_displayName stringByAppendingString:self.surname];
        }
    }
    return _displayName;
}

- (NSString *)description {
    return [self displayName];
}

#pragma mark - Form

- (NSArray *)excludedFields {
    return @[@"contactId"];
}

@end
