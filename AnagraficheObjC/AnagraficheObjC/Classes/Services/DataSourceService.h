//
//  DataSourceService.h
//  AnagraficheObjC
//
//  Created by Damiano Giusti on 16/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Contact.h"

@interface DataSourceService : NSObject

+ (Contact *)contact;

+ (NSMutableArray *)randomContacts:(NSInteger)number;

@end
