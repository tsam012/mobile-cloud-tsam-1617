//
//  DataSourceService.m
//  AnagraficheObjC
//
//  Created by Damiano Giusti on 16/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "DataSourceService.h"

#define RandomUUID [[[NSUUID alloc] init] UUIDString]

@implementation DataSourceService

+ (Contact *)contact {
    return [[Contact alloc] initWithId:RandomUUID];
}

+ (NSMutableArray *)randomContacts:(NSInteger)number {
    NSMutableArray *contacts = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < number; ++i) {
        Contact *contact = [[Contact alloc] initWithId:RandomUUID];
        
        [contact setName:[NSString stringWithFormat:@"Name #%i", (i+1)]];
        [contact setSurname:[NSString stringWithFormat:@"Surname #%i", (i+1)]];
        [contact setBirthDate:[NSDate date]];
        
        [contacts addObject:contact];
    }
    
    return contacts;
}

@end
