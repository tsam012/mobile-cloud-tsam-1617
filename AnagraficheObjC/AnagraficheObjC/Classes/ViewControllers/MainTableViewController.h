//
//  ViewController.h
//  AnagraficheObjC
//
//  Created by Damiano Giusti on 16/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DetailViewController.h"

@interface MainTableViewController : UITableViewController <DetailViewControllerDelegate>

@end

