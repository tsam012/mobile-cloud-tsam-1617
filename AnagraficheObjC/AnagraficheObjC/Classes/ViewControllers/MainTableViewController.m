//
//  ViewController.m
//  AnagraficheObjC
//
//  Created by Damiano Giusti on 16/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "MainTableViewController.h"

#import "Contact.h"
#import "DataSourceService.h"

#define PrintFunction NSLog(@"%s", __PRETTY_FUNCTION__);

@interface MainTableViewController ()

@property (strong, nonatomic) NSMutableArray *contacts;

@end

@implementation MainTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setTitle:@"Anagrafiche"];
    [self.contacts addObjectsFromArray:[DataSourceService randomContacts:0]];
    [self sortContacts];
    
    [self addButtonsInNavigationBar];
}


#pragma mark - Setup View Controller

- (void)addButtonsInNavigationBar {
    UIBarButtonItem *menuItemAdd = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addButtonPressed:)];
    [self.navigationItem setRightBarButtonItem:menuItemAdd];
}


- (void)addButtonPressed:(id)sender {
    PrintFunction
    
    DetailViewController *detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
    [detailVC setContact:nil];
    [detailVC setDelegate:self];
    [self.navigationController pushViewController:detailVC animated:YES];
}


- (void)sortContacts {
    [self.contacts sortUsingComparator:^NSComparisonResult(Contact*  _Nonnull c1, Contact*  _Nonnull c2) {
        return [c1.name compare:c2.name];
    }];
}


- (NSMutableArray *)contacts {
    if (!_contacts)
        _contacts = [[NSMutableArray alloc] init];
    return _contacts;
}


#pragma mark - Table View


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.contacts count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactTableViewCell" forIndexPath:indexPath];
    
    Contact *contact = self.contacts[indexPath.row];
    
    [cell.textLabel setText:[contact displayName]];
    
    return cell;
}


#pragma mark - Delegate


- (void)detailViewControllerDidEditedData {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self sortContacts];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    });
}


- (void)detailViewControllerDidAddContact:(Contact *)contact {
    [self.contacts addObject:contact];
    [self detailViewControllerDidEditedData];
}


#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[DetailViewController class]]) {
        DetailViewController *detailViewController = (DetailViewController *) segue.destinationViewController;
        [detailViewController setContact:self.contacts[self.tableView.indexPathForSelectedRow.row]];
        [detailViewController setDelegate:self];
    }
}

@end
