//
//  DetailViewController.m
//  AnagraficheObjC
//
//  Created by Damiano Giusti on 16/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import "DetailViewController.h"
#import "DataSourceService.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic) BOOL isEditMode;

@end

@implementation DetailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (!self.contact) {
        self.isEditMode = NO;
        [self setTitle:@"Add new contact"];
        self.contact = [DataSourceService contact];
    } else {
        self.isEditMode = YES;
        [self setTitle:[self.contact displayName]];
        NSLog(@"%@", self.contact);
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [self.formController setForm:self.contact];
}


- (void)viewWillDisappear:(BOOL)animated {
    if (self.isEditMode) {
        [self.delegate detailViewControllerDidEditedData];
    } else {
        if ([self.contact.name length] > 0)
            [self.delegate detailViewControllerDidAddContact:self.contact];
    }
}

- (void)upda

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
