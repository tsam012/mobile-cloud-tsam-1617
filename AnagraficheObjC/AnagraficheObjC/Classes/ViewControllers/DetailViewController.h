//
//  DetailViewController.h
//  AnagraficheObjC
//
//  Created by Damiano Giusti on 16/02/17.
//  Copyright © 2017 Damiano Giusti. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Contact.h"

@protocol DetailViewControllerDelegate <NSObject>

- (void)detailViewControllerDidEditedData;

- (void)detailViewControllerDidAddContact:(Contact *)contact;

@end

@interface DetailViewController : FXFormViewController

@property (unsafe_unretained, nonatomic) id <DetailViewControllerDelegate> delegate;

@property (strong, nonatomic) Contact *contact;

@end
