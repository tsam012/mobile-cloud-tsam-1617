package it.damianogiusti.anagrafiche.view;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import it.damianogiusti.anagrafiche.R;

/**
 * Created by Damiano Giusti on 19/01/17.
 */
public abstract class MainActivityHelper extends AppCompatActivity {

    private SearchView searchView;
    private static Handler mainThreadHandler = new Handler(Looper.getMainLooper());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.activity_main_menu, menu);
        searchView = (SearchView) menu.findItem(R.id.menuItemSearchView).getActionView();
        setupSearchView();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.menuItemAdd:
                onAddContactClicked();
                break;
            case R.id.menuItemSearchView:
                searchView.onActionViewExpanded();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified())
            searchView.onActionViewCollapsed();
        else super.onBackPressed();
    }

    private void setupSearchView() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                onSearchTextSubmitted(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                mainThreadHandler.removeCallbacksAndMessages("search");
                mainThreadHandler.postAtTime(new Runnable() {
                    @Override
                    public void run() {
                        onSearchTextChange(newText);
                    }
                }, "search", SystemClock.uptimeMillis() + 300);
                return false;
            }
        });
    }

    protected abstract void onAddContactClicked();

    protected abstract void onSearchTextSubmitted(String query);

    protected abstract void onSearchTextChange(String newText);
}
