package it.damianogiusti.anagrafiche.repository;

import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.content.ContentProvider;
import com.activeandroid.query.Select;

import java.util.List;

import it.damianogiusti.anagrafiche.model.Contact;

/**
 * Created by Damiano Giusti on 15/02/17.
 */
public class ActiveAndroidContactRepository implements ContactRepository {

    private Context context;

    public ActiveAndroidContactRepository(Context context) {
        this.context = context;
    }

    @Override
    public Contact findById(long id) {
        return Contact.load(Contact.class, id);
    }

    @Override
    public List<Contact> findSetById(long[] ids) {

        StringBuilder whereClause = new StringBuilder();
        String idField = Cache.getTableInfo(Contact.class).getIdName();
        whereClause.append(idField).append(" in (");
        for (int i = 0; i < ids.length; i++) {
            long l = ids[i];
            whereClause.append(l);
            if (i + 1 < ids.length)
                whereClause.append(", ");
        }
        whereClause.append(")");
        return new Select().from(Contact.class).where(whereClause.toString()).execute();
    }

    @Override
    public List<Contact> findAll() {
        return new Select().from(Contact.class).execute();
    }

    @Override
    public List<Contact> findAllContaining(String text) {
        final String searchText = text.toLowerCase();
        return new Select().from(Contact.class).where("lowercase(name) = ? OR lowercase(surname) = ?", searchText, searchText).execute();
    }

    @Override
    public Loader<Cursor> getLoaderForAll() {
        return new CursorLoader(context, ContentProvider.createUri(Contact.class, null), null, null, null, null);
    }

    @Override
    public Cursor getCursorForAll() {
        return context.getContentResolver().query(ContentProvider.createUri(Contact.class, null), null, null, null, null);
    }

    @Override
    public Loader<Cursor> getLoaderForAllContaining(String text) {
        return null;
    }

    @Override
    public Cursor getCursorForAllContaining(String text) {
        return null;
    }

    @Override
    public Contact saveOrUpdate(Contact contact) {
        long id = contact.save();
        return contact;
    }

    @Override
    public boolean saveAll(List<Contact> contacts) {
        ActiveAndroid.beginTransaction();
        try {
            for (Contact contact : contacts)
                contact.save();
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return true;
    }

    @Override
    public boolean delete(long id) {
        Model.delete(Contact.class, id);
        return true;
    }
}
