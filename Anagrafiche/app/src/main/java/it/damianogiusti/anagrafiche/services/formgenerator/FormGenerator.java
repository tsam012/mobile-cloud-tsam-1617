package it.damianogiusti.anagrafiche.services.formgenerator;

import android.content.Context;
import android.support.annotation.StyleRes;
import android.view.ContextThemeWrapper;
import android.widget.LinearLayout;

import java.util.List;

/**
 * Created by Damiano Giusti on 08/02/17.
 */
public class FormGenerator {

    static class UIUtils {

        private static void init(Context context) {
            DISPLAY_HEIGHT = context.getResources().getDisplayMetrics().heightPixels;
            DISPLAY_WIDTH = context.getResources().getDisplayMetrics().widthPixels;
            DISPLAY_DENSITY = context.getResources().getDisplayMetrics().density;
        }

        static int DISPLAY_HEIGHT;
        static int DISPLAY_WIDTH;
        private static float DISPLAY_DENSITY;

        public static int dp2px(float dp) {
            return (int) (dp * DISPLAY_DENSITY + 0.5f);
        }
    }

    private static Context context = null;

    static Context getContext() {
        if (context == null)
            throw new IllegalStateException("Init FormGenerator first.");
        return context;
    }

    public static void init(Context context, @StyleRes int themeResId) {
        FormGenerator.context = new ContextThemeWrapper(context, themeResId);
        UIUtils.init(context);
    }

    public static void inflateInContainer(List<? extends Entry> entries, LinearLayout container) {
        container.setOrientation(LinearLayout.VERTICAL);
        for (Entry entry : entries)
            container.addView(entry.getLayout());
    }
}
