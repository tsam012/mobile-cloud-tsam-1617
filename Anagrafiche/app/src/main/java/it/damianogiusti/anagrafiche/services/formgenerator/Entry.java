package it.damianogiusti.anagrafiche.services.formgenerator;

import android.support.annotation.CallSuper;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Damiano Giusti on 08/02/17.
 */
public abstract class Entry {

    protected int id;
    protected CharSequence title;

    public Entry(int id, CharSequence title) {
        this.id = id;
        this.title = title;
    }

    @CallSuper
    ViewGroup getLayout() {
        LinearLayout container = new LinearLayout(FormGenerator.getContext());
        container.setOrientation(LinearLayout.VERTICAL);
        TextView textView = new TextView(FormGenerator.getContext());
        textView.setText(title);
        container.addView(textView);
        return container;
    }
}
