package it.damianogiusti.anagrafiche.view.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import it.damianogiusti.anagrafiche.R;

/**
 * Created by Damiano Giusti on 08/02/17.
 */
public class ConfirmDialog extends DialogFragment {

    public interface ActionsListener {
        void onConfirmedActionOnContact(long id);
    }

    private static final String ID_KEY_FOR_BUNDLE = "idBundled";
    public static final String NAME_KEY_FOR_BUNDLE = "nameBundled";

    public static ConfirmDialog newInstance(long id, String name) {
        ConfirmDialog confirmDialog = new ConfirmDialog();
        Bundle bundle = new Bundle();
        bundle.putLong(ID_KEY_FOR_BUNDLE, id);
        bundle.putString(NAME_KEY_FOR_BUNDLE, name);
        confirmDialog.setArguments(bundle);
        return confirmDialog;
    }

    private ConfirmDialog.ActionsListener actionsListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ConfirmDialog.ActionsListener)
            actionsListener = (ActionsListener) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        long id = Integer.MIN_VALUE;
        String name = "";

        Bundle arguments = getArguments();
        if (arguments != null) {
            id = arguments.getLong(ID_KEY_FOR_BUNDLE);
            name = arguments.getString(NAME_KEY_FOR_BUNDLE);
        }
        final long finalId = id;

        String message = getActivity().getString(R.string.delete_dialog_message, name);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.delete_dialog_title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (actionsListener != null)
                            actionsListener.onConfirmedActionOnContact(finalId);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                });
        return dialogBuilder.create();
    }
}
