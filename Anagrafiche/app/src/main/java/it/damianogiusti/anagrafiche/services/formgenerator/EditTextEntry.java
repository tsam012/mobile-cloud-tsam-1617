package it.damianogiusti.anagrafiche.services.formgenerator;

import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * Created by Damiano Giusti on 08/02/17.
 */
public class EditTextEntry extends Entry {

    private CharSequence text;

    public EditTextEntry(int id, CharSequence title, CharSequence text) {
        super(id, title);
        this.text = text;
    }

    @Override
    ViewGroup getLayout() {
        LinearLayout linearLayout = (LinearLayout) super.getLayout();

        EditText editText = new EditText(FormGenerator.getContext());
        editText.setId(id);
        if (!TextUtils.isEmpty(text))
            editText.setText(text);
        linearLayout.addView(editText);
        return linearLayout;
    }
}
