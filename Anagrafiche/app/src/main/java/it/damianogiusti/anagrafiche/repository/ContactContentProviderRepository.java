package it.damianogiusti.anagrafiche.repository;

import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import it.damianogiusti.anagrafiche.database.ContactsContentProvider;
import it.damianogiusti.anagrafiche.database.tables.ContactTable;
import it.damianogiusti.anagrafiche.model.Contact;
import it.damianogiusti.anagrafiche.repository.helpers.ContactHelper;

/**
 * Created by Damiano Giusti on 01/02/17.
 */
public class ContactContentProviderRepository implements ContactRepository {

    private Context context;

    public ContactContentProviderRepository(Context context) {
        this.context = context;
    }

    @Override
    public Contact findById(long id) {
        Uri uri = Uri.parse(ContactsContentProvider.CONTACTS_URI + "/" + id);
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);

        try {
            if (cursor != null && cursor.getCount() == 1 && cursor.moveToFirst())
                return ContactHelper.getContactFromCursor(cursor);
            return null;
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }

    @Override
    public List<Contact> findSetById(long[] ids) {
        StringBuilder in = new StringBuilder("(");
        for (int i = 0; i < ids.length; i++) {
            long l = ids[i];
            in.append(l);
            if (i < ids.length - 1)
                in.append(", ");
        }
        in.append(")");
        String selection = String.format("%s IN %s", ContactTable._ID, in);

        Cursor cursor = context.getContentResolver().query(ContactsContentProvider.CONTACTS_URI, null, selection, null, null);

        List<Contact> contacts = new ArrayList<>(cursor.getCount());

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                contacts.add(ContactHelper.getContactFromCursor(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return contacts;
    }

    @Override
    public List<Contact> findAll() {
        Cursor cursor = getCursorForAll();

        List<Contact> contacts = new ArrayList<>(cursor.getCount());

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                contacts.add(ContactHelper.getContactFromCursor(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return contacts;
    }

    @Override
    public List<Contact> findAllContaining(String text) {
        Cursor cursor = getCursorForAllContaining(text);

        List<Contact> contacts = new ArrayList<>(cursor.getCount());

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                contacts.add(ContactHelper.getContactFromCursor(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();
        return contacts;
    }

    @Override
    public Loader<Cursor> getLoaderForAll() {
        return new CursorLoader(context, ContactsContentProvider.CONTACTS_URI, null, null, null, null);
    }

    @Override
    public Cursor getCursorForAll() {
        return context.getContentResolver().query(ContactsContentProvider.CONTACTS_URI, null, null, null, null);
    }

    @Override
    public Loader<Cursor> getLoaderForAllContaining(String text) {
        final String selection = String.format("lower(%1$s) LIKE '%%%3$s%%' OR lower(%2$s) LIKE '%%%3$s%%'", ContactTable.NAME, ContactTable.SURNAME, text);
        return new CursorLoader(context, ContactsContentProvider.CONTACTS_URI, null, selection, null, null);

    }

    @Override
    public Cursor getCursorForAllContaining(String text) {
        final String selection = String.format("lower(%1$s) LIKE '%%%3$s%%' OR lower(%2$s) LIKE '%%%3$s%%'", ContactTable.NAME, ContactTable.SURNAME, text);
        return context.getContentResolver().query(ContactsContentProvider.CONTACTS_URI, null, selection, null, null);
    }

    @Override
    public Contact saveOrUpdate(Contact contact) {
        ContentValues values = ContactHelper.getContentValuesForContact(contact);
        if (contact.getId() == Integer.MIN_VALUE) {
            Uri insertedUri = context.getContentResolver().insert(ContactsContentProvider.CONTACTS_URI, values);
//            if (insertedUri != null)
//                contact.setId(Long.parseLong(insertedUri.getLastPathSegment()));
        } else
            context.getContentResolver().update(Uri.parse(ContactsContentProvider.CONTACTS_URI + "/" + contact.getId()), values, null, null);
        return contact;
    }

    @Override
    public boolean saveAll(List<Contact> contacts) {
        List<Contact> savedContacts = new ArrayList<>(contacts);
        for (Contact contact : contacts)
            savedContacts.add(saveOrUpdate(contact));
        return true;
    }

    @Override
    public boolean delete(long id) {
        return context.getContentResolver().delete(Uri.parse(ContactsContentProvider.CONTACTS_URI + "/" + id), null, null) == 1;
    }
}
