package it.damianogiusti.anagrafiche.repository;

import android.content.Context;

/**
 * Created by Damiano Giusti on 15/02/17.
 */
public class ContactRepositoryFactory {

    public static ContactRepository getContactRepository(Context context) {
        return new ActiveAndroidContactRepository(context);
    }
}
