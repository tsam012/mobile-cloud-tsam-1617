package it.damianogiusti.anagrafiche.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;

import it.damianogiusti.anagrafiche.R;
import it.damianogiusti.anagrafiche.model.Contact;
import it.damianogiusti.anagrafiche.repository.ContactContentProviderRepository;
import it.damianogiusti.anagrafiche.repository.ContactRepositoryFactory;
import it.damianogiusti.anagrafiche.repository.helpers.ContactHelper;
import it.damianogiusti.anagrafiche.services.Cat;

/**
 * Created by Damiano Giusti on 19/01/17.
 */
public class ContactsAdapter extends CursorAdapter {

    public interface ItemEditActionsListener {
        void onDeleteRequested(Contact contact);

        void onUpdateRequested(Contact contact);
    }

    private LayoutInflater inflater;
    private ItemEditActionsListener editActionsListener;

    public ContactsAdapter(Context context) {
        super(context, null, false);
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = inflater.inflate(R.layout.contact_list_item_layout, parent, false);
        ContactViewHolder viewHolder = new ContactViewHolder();
        viewHolder.imgAvatar = (ImageView) view.findViewById(R.id.imgAvatar);
        viewHolder.txtDisplayName = (TextView) view.findViewById(R.id.txtDisplayName);
        viewHolder.swipeLayout = (SwipeLayout) view.findViewById(R.id.swipeLayout);
        viewHolder.btnUpdate = (Button) view.findViewById(R.id.btnUpdate);
        viewHolder.btnDelete = (Button) view.findViewById(R.id.btnDelete);

        view.setTag(viewHolder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final ContactViewHolder viewHolder = (ContactViewHolder) view.getTag();
        final Contact contact = ContactHelper.getContactFromCursor(cursor);
        viewHolder.swipeLayout.close(false);
        viewHolder.txtDisplayName.setText(String.format("%s %s", contact.getName(), contact.getSurname()));
        viewHolder.btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editActionsListener != null)
                    editActionsListener.onUpdateRequested(contact);
                viewHolder.swipeLayout.close();
            }
        });
        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editActionsListener != null)
                    editActionsListener.onDeleteRequested(contact);
                viewHolder.swipeLayout.close();
            }
        });
        Cat.meowInto(viewHolder.imgAvatar, String.valueOf(contact.getId()));
    }

    public void setEditActionsListener(ItemEditActionsListener editActionsListener) {
        this.editActionsListener = editActionsListener;
    }

    private static class ContactViewHolder {
        private ImageView imgAvatar;
        private TextView txtDisplayName;
        private SwipeLayout swipeLayout;
        private Button btnUpdate;
        private Button btnDelete;
    }
}
