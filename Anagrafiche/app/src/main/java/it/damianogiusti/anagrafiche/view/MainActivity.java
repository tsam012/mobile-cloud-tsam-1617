package it.damianogiusti.anagrafiche.view;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.Date;
import java.util.Random;

import it.damianogiusti.anagrafiche.R;
import it.damianogiusti.anagrafiche.adapter.ContactsAdapter;
import it.damianogiusti.anagrafiche.model.Contact;
import it.damianogiusti.anagrafiche.repository.ContactContentProviderRepository;
import it.damianogiusti.anagrafiche.repository.ContactRepository;
import it.damianogiusti.anagrafiche.repository.ContactRepositoryFactory;
import it.damianogiusti.anagrafiche.view.dialogs.ConfirmDialog;

public class MainActivity extends MainActivityHelper implements
        LoaderManager.LoaderCallbacks<Cursor>,
        ContactsAdapter.ItemEditActionsListener,
        ConfirmDialog.ActionsListener {

    private ListView listView;
    private ContactsAdapter contactsAdapter;
    private ContactRepository contactRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listView = (ListView) findViewById(R.id.listView);
        contactsAdapter = new ContactsAdapter(getApplicationContext());
        contactRepository = ContactRepositoryFactory.getContactRepository(getApplicationContext());

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra(DetailActivity.CONTACT_ID_KEY_FOR_BUNDLE, id);
                intent.putExtra(DetailActivity.UPDATE_KEY_FOR_BUNDLE, false);
                startActivity(intent);
            }
        });

        getLoaderManager().initLoader(hashCode(), null, this);

        contactsAdapter.setEditActionsListener(this);
        listView.setAdapter(contactsAdapter);
    }

    @Override
    protected void onAddContactClicked() {
        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra(DetailActivity.UPDATE_KEY_FOR_BUNDLE, true);
        startActivity(intent);
    }

    @Override
    protected void onSearchTextSubmitted(String query) {
        onSearchTextChange(query);
    }

    @Override
    protected void onSearchTextChange(String newText) {
        if (!TextUtils.isEmpty(newText.trim()))
            contactsAdapter.swapCursor(contactRepository.getCursorForAllContaining(newText.toLowerCase()));
        else contactsAdapter.swapCursor(contactRepository.getCursorForAll());
    }

    public static Contact createRandomContact() {
        Random random = new Random();

        int x = random.nextInt(50);
        Contact contact = new Contact();
        contact.setName("nome " + x);
        contact.setSurname("cognome " + x);
        contact.setBirthDate(new Date());
        contact.setEmail("mail " + x);
        contact.setPhoneNumber("call " + x);
        contact.setAddress("address " + x);
        contact.setAddressNumber("" + x);
        contact.setZipCode("" + x);
        contact.setCity("city " + x);
        contact.setProvince("province " + x);
        long latitude = x + 1;
        long longitude = x + 10;

        return contact;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return contactRepository.getLoaderForAll();
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        contactsAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        contactsAdapter.swapCursor(null);
    }

    @Override
    public void onDeleteRequested(Contact contact) {
        ConfirmDialog.newInstance(contact.getId(), contact.getName())
                .show(getFragmentManager(), null);
    }

    @Override
    public void onUpdateRequested(Contact contact) {
        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra(DetailActivity.CONTACT_ID_KEY_FOR_BUNDLE, contact.getId());
        intent.putExtra(DetailActivity.UPDATE_KEY_FOR_BUNDLE, true);
        startActivity(intent);
    }

    @Override
    public void onConfirmedActionOnContact(long id) {
        contactRepository.delete(id);
    }
}
