package it.damianogiusti.anagrafiche.repository.helpers;

import android.content.ContentValues;
import android.database.Cursor;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Cache;
import com.activeandroid.Model;

import java.util.Date;

import it.damianogiusti.anagrafiche.database.tables.ContactTable;
import it.damianogiusti.anagrafiche.model.Contact;

/**
 * Created by Damiano Giusti on 15/02/17.
 */
public class ContactHelper {

    public static Contact getContactFromCursor(Cursor cursor) {
        Contact contact = new Contact();
        contact.loadFromCursor(cursor);
      /*  contact.setId(cursor.getLong(cursor.getColumnIndex(ContactTable._ID)));
        contact.setName(cursor.getString(cursor.getColumnIndex(ContactTable.NAME)));
        contact.setSurname(cursor.getString(cursor.getColumnIndex(ContactTable.SURNAME)));
        contact.setBirthDate(new Date(cursor.getLong(cursor.getColumnIndex(ContactTable.BIRTH_DATE))));
        contact.setEmail(cursor.getString(cursor.getColumnIndex(ContactTable.EMAIL)));
        contact.setPhoneNumber(cursor.getString(cursor.getColumnIndex(ContactTable.PHONE_NUMBER)));
        contact.setAddress(cursor.getString(cursor.getColumnIndex(ContactTable.ADDRESS)));
        contact.setAddressNumber(cursor.getString(cursor.getColumnIndex(ContactTable.ADDRESS_NUMBER)));
        contact.setCity(cursor.getString(cursor.getColumnIndex(ContactTable.CITY)));
        contact.setZipCode(cursor.getString(cursor.getColumnIndex(ContactTable.ZIP_CODE)));
        contact.setProvince(cursor.getString(cursor.getColumnIndex(ContactTable.PROVINCE)));
        contact.setLatitude(cursor.getDouble(cursor.getColumnIndex(ContactTable.LATITUDE)));
        contact.setLongitude(cursor.getDouble(cursor.getColumnIndex(ContactTable.LONGITUDE)));*/
        return contact;
    }

    public static ContentValues getContentValuesForContact(Contact contact) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ContactTable.NAME, contact.getName());
        contentValues.put(ContactTable.SURNAME, contact.getSurname());
        contentValues.put(ContactTable.BIRTH_DATE,
                contact.getBirthDate() != null ? contact.getBirthDate().getTime() : null);
        contentValues.put(ContactTable.EMAIL, contact.getEmail());
        contentValues.put(ContactTable.PHONE_NUMBER, contact.getPhoneNumber());
        contentValues.put(ContactTable.ADDRESS, contact.getAddress());
        contentValues.put(ContactTable.ADDRESS_NUMBER, contact.getAddressNumber());
        contentValues.put(ContactTable.CITY, contact.getCity());
        contentValues.put(ContactTable.ZIP_CODE, contact.getZipCode());
        contentValues.put(ContactTable.PROVINCE, contact.getProvince());
        contentValues.put(ContactTable.LATITUDE, contact.getLatitude());
        contentValues.put(ContactTable.LONGITUDE, contact.getLongitude());
        return contentValues;
    }
}
