package it.damianogiusti.anagrafiche.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.Date;

import it.damianogiusti.anagrafiche.database.tables.ContactTable;

/**
 * Created by Damiano Giusti on 19/01/17.
 */
@Table(name = "contacts", id = ContactTable._ID)
public class Contact extends Model {

    @Column(name = ContactTable.NAME)
    private String name;

    @Column(name = ContactTable.SURNAME)
    private String surname;

    @Column(name = ContactTable.BIRTH_DATE)
    private Date birthDate;

    @Column(name = ContactTable.EMAIL)
    private String email;

    @Column(name = ContactTable.PHONE_NUMBER)
    private String phoneNumber;

    @Column(name = ContactTable.ADDRESS)
    private String address;

    @Column(name = ContactTable.CITY)
    private String city;

    @Column(name = ContactTable.ZIP_CODE)
    private String zipCode;

    @Column(name = ContactTable.PROVINCE)
    private String province;

    @Column(name = ContactTable.LATITUDE)
    private double latitude;

    @Column(name = ContactTable.LONGITUDE)
    private double longitude;

    @Column(name = ContactTable.ADDRESS_NUMBER)
    private String addressNumber;

    public Contact() {
        super();
    }

    public Contact(Contact contact) {
        super();
        this.name = contact.name;
        this.surname = contact.surname;
        this.birthDate = contact.birthDate;
        this.email = contact.email;
        this.phoneNumber = contact.phoneNumber;
        this.address = contact.address;
        this.city = contact.city;
        this.zipCode = contact.zipCode;
        this.province = contact.province;
        this.latitude = contact.latitude;
        this.longitude = contact.longitude;
        this.addressNumber = contact.addressNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zip_code) {
        this.zipCode = zip_code;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAddressNumber() {
        return addressNumber;
    }

    public void setAddressNumber(String address_number) {
        this.addressNumber = address_number;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof Contact))
            return false;

        Contact contact = (Contact) obj;
        return getId() == contact.getId();
    }

    @Override
    public String toString() {
        return String.format("%s %s", name, surname);
    }
}
