package it.damianogiusti.anagrafiche.services;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import it.damianogiusti.anagrafiche.MainApplication;

/**
 * Created by Damiano Giusti on 08/02/17.
 */
public final class Cat {

    public static void meowInto(final ImageView target, final String id) {
        target.post(new Runnable() {
            @Override
            public void run() {
                Picasso.with(MainApplication.getGlobalContext())
                        .load("https://thecatapi.com/api/images/get?format=src")
                        .stableKey(id)
                        .resize(target.getWidth(), target.getHeight())
                        .centerInside()
                        .into(target);
            }
        });
    }
}
