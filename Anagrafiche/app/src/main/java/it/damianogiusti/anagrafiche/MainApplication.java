package it.damianogiusti.anagrafiche;

import android.app.Application;
import android.content.Context;

import com.activeandroid.ActiveAndroid;

import java.lang.ref.WeakReference;

import it.damianogiusti.anagrafiche.services.formgenerator.FormGenerator;

/**
 * Created by Damiano Giusti on 08/02/17.
 */
public class MainApplication extends Application {

    private static WeakReference<Context> weakContext = null;

    public static Context getGlobalContext() {
        return weakContext.get();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        weakContext = new WeakReference<>(getApplicationContext());
        FormGenerator.init(getApplicationContext(), R.style.AppTheme);
        ActiveAndroid.initialize(this);
    }
}
