package it.damianogiusti.anagrafiche.database.tables;

import android.provider.BaseColumns;

/**
 * Created by Damiano Giusti on 19/01/17.
 */
public class ContactTable implements BaseColumns {

    public static final String TABLE_NAME = "contacts";

    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String BIRTH_DATE = "birth_date";
    public static final String EMAIL = "email";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String ADDRESS = "address";
    public static final String ADDRESS_NUMBER = "address_number";
    public static final String CITY = "city";
    public static final String ZIP_CODE = "zip_code";
    public static final String PROVINCE = "province";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    public static final String CREATE_TABLE_STATEMENT =
            "CREATE TABLE " + TABLE_NAME + "(" +
                    _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    NAME + " TEXT NOT NULL, " +
                    SURNAME + " TEXT NOT NULL, " +
                    BIRTH_DATE + " INTEGER, " +
                    EMAIL + " TEXT, " +
                    PHONE_NUMBER + " TEXT, " +
                    ADDRESS + " TEXT, " +
                    ADDRESS_NUMBER + " TEXT, " +
                    CITY + " TEXT, " +
                    ZIP_CODE + " TEXT, " +
                    PROVINCE + " TEXT, " +
                    LATITUDE + " REAL, " +
                    LONGITUDE + " REAL" +
                    ");";

    public static final String DROP_TABLE_STATEMENT = "DROP TABLE " + TABLE_NAME;
}
