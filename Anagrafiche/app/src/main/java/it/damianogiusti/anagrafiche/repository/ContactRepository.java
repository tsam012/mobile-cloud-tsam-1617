package it.damianogiusti.anagrafiche.repository;

import android.content.Loader;
import android.database.Cursor;

import java.util.List;

import it.damianogiusti.anagrafiche.model.Contact;

/**
 * Created by Damiano Giusti on 19/01/17.
 */
public interface ContactRepository {

    class ContactChangeEvent {
    }

    Contact findById(long id);

    List<Contact> findSetById(long[] id);

    List<Contact> findAll();

    List<Contact> findAllContaining(String text);

    Loader<Cursor> getLoaderForAll();

    Cursor getCursorForAll();

    Loader<Cursor> getLoaderForAllContaining(String text);

    Cursor getCursorForAllContaining(String text);

    Contact saveOrUpdate(Contact contact);

    boolean saveAll(List<Contact> contacts);

    boolean delete(long id);
}
