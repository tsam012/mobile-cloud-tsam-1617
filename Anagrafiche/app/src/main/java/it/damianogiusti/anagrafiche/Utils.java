package it.damianogiusti.anagrafiche;

/**
 * Created by Damiano Giusti on 08/02/17.
 */
public class Utils {

    public static final int DISPLAY_HEIGHT = MainApplication.getGlobalContext().getResources().getDisplayMetrics().heightPixels;
    public static final int DISPLAY_WIDTH = MainApplication.getGlobalContext().getResources().getDisplayMetrics().widthPixels;
    private static final float DISPLAY_DENSITY = MainApplication.getGlobalContext().getResources().getDisplayMetrics().density;

    public static int dp2px(float dp) {
        return (int) (dp * DISPLAY_DENSITY + 0.5f);
    }
}
