package it.damianogiusti.anagrafiche.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import it.damianogiusti.anagrafiche.R;
import it.damianogiusti.anagrafiche.model.Contact;
import it.damianogiusti.anagrafiche.repository.ContactRepository;
import it.damianogiusti.anagrafiche.repository.ContactRepositoryFactory;
import it.damianogiusti.anagrafiche.services.Cat;
import it.damianogiusti.anagrafiche.services.formgenerator.EditTextEntry;
import it.damianogiusti.anagrafiche.services.formgenerator.Entry;
import it.damianogiusti.anagrafiche.services.formgenerator.FormGenerator;
import it.damianogiusti.anagrafiche.services.formgenerator.TextViewEntry;

public class DetailActivity extends AppCompatActivity {

    public static final String CONTACT_ID_KEY_FOR_BUNDLE = "contactIdBundled";
    public static final String UPDATE_KEY_FOR_BUNDLE = "updateBundled";

    private enum EntriesIDs {
        NAME,
        SURNAME,
        BIRTH_DATE,
        EMAIL,
        PHONE,
        ADDRESS,
        ADDRESS_NUMBER,
        CITY,
        ZIP,
        PROVINCE
    }

    private ImageView imgAvatar;
    private LinearLayout formContainer;
    private TextView textViews[];
    private EditText editTexts[];

    private Contact contact;
    private boolean isUpdate;
    private ContactRepository contactRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        bindViews();
        // display up navigation
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        long id;
        if (savedInstanceState == null) {
            id = getIntent().getLongExtra(CONTACT_ID_KEY_FOR_BUNDLE, Integer.MIN_VALUE);
            isUpdate = id == Integer.MIN_VALUE || getIntent().getBooleanExtra(UPDATE_KEY_FOR_BUNDLE, false);
        } else {
            id = savedInstanceState.getLong(CONTACT_ID_KEY_FOR_BUNDLE, Integer.MIN_VALUE);
            isUpdate = id == Integer.MIN_VALUE || savedInstanceState.getBoolean(UPDATE_KEY_FOR_BUNDLE, false);
        }

        contactRepository = ContactRepositoryFactory.getContactRepository(getApplicationContext());
        contact = contactRepository.findById(id);
        if (contact == null)
            contact = new Contact();

        Cat.meowInto(imgAvatar, String.valueOf(contact.getId()));
        createForm();
        bindFormViews();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(CONTACT_ID_KEY_FOR_BUNDLE, contact.getId());
        outState.putBoolean(UPDATE_KEY_FOR_BUNDLE, isUpdate);
    }

    private void bindViews() {
        imgAvatar = (ImageView) findViewById(R.id.imgAvatar);
        formContainer = (LinearLayout) findViewById(R.id.formContainer);
    }

    private void bindFormViews() {
        final int entriesCount = EntriesIDs.values().length;
        if (isUpdate) {
            editTexts = new EditText[entriesCount];
            textViews = new TextView[0];
            for (int i = 0; i < entriesCount; i++)
                editTexts[i] = (EditText) formContainer.findViewById(i);
        } else {
            editTexts = new EditText[0];
            textViews = new TextView[entriesCount];
            for (int i = 0; i < entriesCount; i++)
                textViews[i] = (TextView) formContainer.findViewById(i);
        }
    }

    private void createForm() {
        List<? extends Entry> entries;
        if (isUpdate) {
            entries = Arrays.asList(
                    new EditTextEntry(EntriesIDs.NAME.ordinal(), getString(R.string.name), contact.getName()),
                    new EditTextEntry(EntriesIDs.SURNAME.ordinal(), getString(R.string.surname), contact.getSurname()),
                    new EditTextEntry(EntriesIDs.BIRTH_DATE.ordinal(), getString(R.string.birth_date),
                            contact.getBirthDate() != null ? contact.getBirthDate().toString() : ""),
                    new EditTextEntry(EntriesIDs.EMAIL.ordinal(), getString(R.string.email), contact.getEmail()),
                    new EditTextEntry(EntriesIDs.PHONE.ordinal(), getString(R.string.phone_number), contact.getPhoneNumber()),
                    new EditTextEntry(EntriesIDs.ADDRESS.ordinal(), getString(R.string.address), contact.getAddress()),
                    new EditTextEntry(EntriesIDs.ADDRESS_NUMBER.ordinal(), getString(R.string.address_number), contact.getAddressNumber()),
                    new EditTextEntry(EntriesIDs.CITY.ordinal(), getString(R.string.city), contact.getCity()),
                    new EditTextEntry(EntriesIDs.ZIP.ordinal(), getString(R.string.zip), contact.getZipCode()),
                    new EditTextEntry(EntriesIDs.PROVINCE.ordinal(), getString(R.string.province), contact.getProvince())
            );
        } else {
            entries = Arrays.asList(
                    new TextViewEntry(EntriesIDs.NAME.ordinal(), getString(R.string.name), contact.getName()),
                    new TextViewEntry(EntriesIDs.SURNAME.ordinal(), getString(R.string.surname), contact.getSurname()),
                    new TextViewEntry(EntriesIDs.BIRTH_DATE.ordinal(), getString(R.string.birth_date),
                            contact.getBirthDate() != null ? contact.getBirthDate().toString() : ""),
                    new TextViewEntry(EntriesIDs.EMAIL.ordinal(), getString(R.string.email), contact.getEmail()),
                    new TextViewEntry(EntriesIDs.PHONE.ordinal(), getString(R.string.phone_number), contact.getPhoneNumber()),
                    new TextViewEntry(EntriesIDs.ADDRESS.ordinal(), getString(R.string.address), contact.getAddress()),
                    new TextViewEntry(EntriesIDs.ADDRESS_NUMBER.ordinal(), getString(R.string.address_number), contact.getAddressNumber()),
                    new TextViewEntry(EntriesIDs.CITY.ordinal(), getString(R.string.city), contact.getCity()),
                    new TextViewEntry(EntriesIDs.ZIP.ordinal(), getString(R.string.zip), contact.getZipCode()),
                    new TextViewEntry(EntriesIDs.PROVINCE.ordinal(), getString(R.string.province), contact.getProvince())
            );
        }
        FormGenerator.inflateInContainer(entries, formContainer);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isUpdate) {
            getMenuInflater().inflate(R.menu.detail_update_menu, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.menuItemUpdateConfirm:
                updateContact();
                Toast.makeText(DetailActivity.this, getString(R.string.contact_updated_response), Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }

    private void updateContact() {
//        contact.setId(this.contact.getId());
        contact.setName(getTextForEditText(EntriesIDs.NAME));
        contact.setSurname(getTextForEditText(EntriesIDs.SURNAME));

        // TODO: 08/02/17 birth date
        contact.setBirthDate(this.contact.getBirthDate());

        contact.setEmail(getTextForEditText(EntriesIDs.EMAIL));
        contact.setPhoneNumber(getTextForEditText(EntriesIDs.PHONE));
        contact.setAddress(getTextForEditText(EntriesIDs.ADDRESS));
        contact.setAddressNumber(getTextForEditText(EntriesIDs.ADDRESS_NUMBER));
        contact.setCity(getTextForEditText(EntriesIDs.CITY));
        contact.setZipCode(getTextForEditText(EntriesIDs.ZIP));
        contact.setProvince(getTextForEditText(EntriesIDs.PROVINCE));
        this.contact = contactRepository.saveOrUpdate(contact);
    }

    private String getTextForEditText(EntriesIDs id) {
        return editTexts[id.ordinal()].getText().toString();
    }
}
