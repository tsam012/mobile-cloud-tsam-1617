package it.damianogiusti.anagrafiche.services.formgenerator;

import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Damiano Giusti on 08/02/17.
 */
public class TextViewEntry extends Entry {

    private CharSequence text;


    public TextViewEntry(int id, CharSequence title, CharSequence text) {
        super(id, title);
        this.text = text;
    }

    @Override
    ViewGroup getLayout() {
        LinearLayout linearLayout = (LinearLayout) super.getLayout();
        TextView textView = new TextView(FormGenerator.getContext());
        textView.setTextSize(FormGenerator.UIUtils.dp2px(6));
        textView.setId(id);
        textView.setText(text);
        textView.setPaddingRelative(0, 0, 0, FormGenerator.UIUtils.dp2px(8));
        linearLayout.addView(textView);
        return linearLayout;
    }
}
