package it.damianogiusti.anagrafiche.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import it.damianogiusti.anagrafiche.database.tables.ContactTable;

/**
 * Created by Damiano Giusti on 19/01/17.
 */
public final class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "ANDROID-AIV.db";
    private static final int VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ContactTable.CREATE_TABLE_STATEMENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(ContactTable.DROP_TABLE_STATEMENT);
        db.execSQL(ContactTable.CREATE_TABLE_STATEMENT);
    }
}
