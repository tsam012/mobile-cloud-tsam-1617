package it.damianogiusti.anagrafiche.database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import it.damianogiusti.anagrafiche.database.tables.ContactTable;

/**
 * Created by Damiano Giusti on 01/02/17.
 */
public final class ContactsContentProvider extends ContentProvider {

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/contacts";
    public static final String ITEM_CONTENT_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/contact";

    private static final String AUTHORITY = "it.damianogiusti.anagrafiche.database.CONTACTS";
    private static final String BASE_PATH_CONTACTS = "contacts";

    public static final Uri CONTACTS_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH_CONTACTS);

    // uri matcher configuration
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static final int CONTACTS = 1000;
    private static final int SINGLE_CONTACT = 2000;

    static {
        uriMatcher.addURI(AUTHORITY, BASE_PATH_CONTACTS, CONTACTS);
        uriMatcher.addURI(AUTHORITY, BASE_PATH_CONTACTS + "/#", SINGLE_CONTACT);
    }

    private static final String TABLE_NAME = ContactTable.TABLE_NAME;

    private DatabaseHelper databaseHelper;

    @Override
    public boolean onCreate() {
        databaseHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        SQLiteQueryBuilder queryBuilder = getQueryBuilder();

        switch (getUriType(uri)) {
            case CONTACTS:
                break;
            case SINGLE_CONTACT:
                queryBuilder.appendWhere(String.format("%s = %s", ContactTable._ID, uri.getLastPathSegment()));
                break;
            default:
                return null;
        }

        Cursor cursor = queryBuilder.query(database, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), CONTACTS_URI);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (getUriType(uri)) {
            case CONTACTS:
                return CONTENT_TYPE;
            case SINGLE_CONTACT:
                return ITEM_CONTENT_TYPE;
        }
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        long insertedID;
        switch (getUriType(uri)) {
            case CONTACTS:
                insertedID = database.insert(TABLE_NAME, null, values);
                break;
            default:
                return null;
        }
        if (insertedID >= 0) {
            notifyChange(CONTACTS_URI);
            return Uri.parse(CONTACTS_URI + "/" + insertedID);
        }
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        int deletedCount;
        switch (getUriType(uri)) {
            case CONTACTS:
                deletedCount = database.delete(TABLE_NAME, selection, selectionArgs);
                break;
            case SINGLE_CONTACT:
                if (!TextUtils.isEmpty(selection))
                    deletedCount = database.delete(TABLE_NAME,
                            String.format("%s = %s AND %s", ContactTable._ID, uri.getLastPathSegment(), selection),
                            selectionArgs);
                else deletedCount = database.delete(TABLE_NAME,
                        String.format("%s = %s", ContactTable._ID, uri.getLastPathSegment()),
                        selectionArgs);
                break;
            default:
                return 0;
        }
        if (deletedCount > 0)
            notifyChange(CONTACTS_URI);
        return deletedCount;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        int updatedCount;
        switch (getUriType(uri)) {
            case SINGLE_CONTACT:
                if (!TextUtils.isEmpty(selection))
                updatedCount = database.update(TABLE_NAME,
                        values,
                        String.format("%s = %s AND %s", ContactTable._ID, uri.getLastPathSegment(), selection),
                        selectionArgs);
                else updatedCount = database.update(TABLE_NAME,
                        values,
                        String.format("%s = %s", ContactTable._ID, uri.getLastPathSegment()),
                        selectionArgs);
                break;
            default:
                return 0;
        }
        if (updatedCount > 0)
            notifyChange(CONTACTS_URI);
        return updatedCount;
    }

    private int getUriType(Uri uri) {
        return uriMatcher.match(uri);
    }

    private SQLiteQueryBuilder getQueryBuilder() {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TABLE_NAME);
        return queryBuilder;
    }

    private void notifyChange(Uri uri) {
        Context context = getContext();
        if (context != null) {
            context.getContentResolver().notifyChange(uri, null);
        }
    }
}
