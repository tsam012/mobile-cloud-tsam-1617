package it.damianogiusti.pushtest.firebase;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by damianogiusti on 12/01/17.
 */

public class IncomingNotificationsService extends FirebaseMessagingService {

    private static final String TAG = "NotificationsService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        RemoteMessage.Notification remoteMessageNotification = remoteMessage.getNotification();
        if (remoteMessageNotification == null) {
            Log.e(TAG, "received RemoteMessage had null notification" );
            return;
        }

        String body = remoteMessageNotification.getBody();
        if (body == null) {
            Log.e(TAG, "received notification had null body");
            return;
        }

        EventBus.getDefault().post(NotificationParser.from(body).send(getApplicationContext()));
    }
}
