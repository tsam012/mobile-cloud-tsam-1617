package it.damianogiusti.pushtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Date;

import it.damianogiusti.pushtest.firebase.NotificationParser;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG, "TOKEN: " + FirebaseInstanceId.getInstance().getToken());

        textView = (TextView) findViewById(R.id.textView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNotificationReceived(NotificationParser notificationParser) {
        StringBuilder stringBuilder = new StringBuilder(textView.getText());
        stringBuilder.append(new Date()).append("\n");
        stringBuilder.append(notificationParser.getPayload()).append("\n");
        stringBuilder.append("--------------------------------").append("\n");
        textView.setText(stringBuilder.toString());
    }
}
