package it.damianogiusti.pushtest.firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import it.damianogiusti.pushtest.R;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by damianogiusti on 12/01/17.
 */

public class NotificationParser {
    public static NotificationParser from(String body) {
        return new NotificationParser(body);
    }

    private static final String TITLE_FIELD = "title";
    private static final String SUBTITLE_FIELD = "subtitle";

    private int id;
    private String title;
    private String subtitle;
    private String payload;

    private NotificationParser(String body) {
        this.payload = body;
        this.id = body.hashCode();
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(body);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        this.title = jsonObject.optString(TITLE_FIELD, "");
        this.subtitle = jsonObject.optString(SUBTITLE_FIELD, "");
    }

    public NotificationParser send(Context context) {
        Notification notification = new Notification.Builder(context)
                .setContentTitle(title)
                .setContentText(subtitle)
                .setSmallIcon(R.mipmap.ic_launcher)
                .build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(id, notification);
        return this;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getPayload() {
        return payload;
    }
}
